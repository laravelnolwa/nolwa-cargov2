<?php
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    //Update Routes
    Route::resource('catalogs', 'CatalogController', [
        'as' => 'admin'
    ]);
    Route::get('catalogs/delete/{catalog}', 'CatalogController@destroy')->name('admin.catalogs.delete-catalog');
});
