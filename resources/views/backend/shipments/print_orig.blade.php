<?php 
use \Milon\Barcode\DNS1D;
$d = new DNS1D();
?>

@php
                        $code = filter_var($shipment->code, FILTER_SANITIZE_NUMBER_INT);
                    @endphp
@extends('backend.layouts.app')

@section('content')

<style media="print">
    .no-print, div#kt_header_mobile, div#kt_header, div#kt_footer{
        display: none;
    }
</style>
<!--begin::Entry-->
<div class="box">
    <div class="box-header">
            <div class="box-icon">
            
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12" >
                                   
                <div class="print-only1 col-xs-12 text-center padding010">
                    @if(get_setting('system_logo_white') != null)
                                    <img src="{{ uploaded_asset(get_setting('system_logo_white')) }}" class="d-block mb-5">
                                @else
                                    <img src="{{ static_asset('assets/img/logo.svg') }}" class="d-block mb-5">
                                @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4 ">
                    <table  class="table table-bordered print-table order-table">
                        <tr><th>dfgdfg</th></tr>
                    </table>
                </div>
                <div class="col-md-4 ">
                <table  class="table table-bordered print-table order-table">
                        <tr><th>dfgdfg</th></tr>
                    </table>
                </div>
                <div class="col-md-4 ">
                <table  class="table table-bordered print-table order-table">
                        <tr><th>dfgdfg</th></tr>
                    </table>
                </div>
           
            </div>
        </div>

    </div>
 </div>


 
@endsection
@section('script')
<script>
window.onload = function() {
	javascript:window.print();
};
</script>
@endsection