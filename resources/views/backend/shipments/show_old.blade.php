<?php
use \Milon\Barcode\DNS1D;
$d = new DNS1D();
?>
@extends('backend.layouts.app')


@section('sub_title'){{translate('Shipment')}} {{$shipment->code}}@endsection
@section('subheader')
    <!--begin::Subheader-->
    <div class="py-2 subheader py-lg-6 subheader-solid" id="kt_subheader">
        <div class="flex-wrap container-fluid d-flex align-items-center justify-content-between flex-sm-nowrap">
            <!--begin::Info-->
            <div class="flex-wrap mr-1 d-flex align-items-center">
                <!--begin::Page Heading-->
                <div class="flex-wrap mr-5 d-flex align-items-baseline">
                    <!--begin::Page Title-->
                    <h5 class="my-1 mr-5 text-dark font-weight-bold">{{translate('Shipment')}} {{$shipment->code}}</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="p-0 my-2 mr-5 breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('admin.dashboard')}}" class="text-muted">{{translate('Dashboard')}}</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">{{$shipment->code}}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection

@section('content')


<!--begin::Card-->
<div class="card card-custom gutter-b">
    <div class="p-0 card-body">
        <!-- begin: Invoice-->
        <!-- begin: Invoice header-->
        <div class="px-8 py-8 row justify-content-center pt-md-27 px-md-0">
            <div class="col-md-10">
                <div class="pb-10 d-flex justify-content-between pb-md-20 flex-column flex-md-row">
                <div class="brand-logo">
                    @if(get_setting('system_logo_white') != null)
                        <img  class="b-logo"src="{{ uploaded_asset(get_setting('system_logo_white')) }}" alt="{{$shipment->branch->name}}{{$shipment->branch->email}}" >
                        @else
                            <img src="{{ static_asset('assets/img/logo.svg') }}" class="b-logo">
                        @endif
                    <h3 class="company-name"> {{$shipment->branch->name}} , {{$shipment->branch->email}} ,{{$shipment->branch->responsible_mobile}} </h3>
                <h4  style="direction: rtl;">فاتورة ضريبية</h4>
                    <h4 class="company-name">Tax Invoice</h4>
            </div>
        </div>
        <div class="well well-sm">
                        <div class="col-md-8 padding010" style="float: left">
                            <div class="clearfix"></div>
                                <div class="table-responsive">
                                    <table class="table table-bordered print-table order-table">
                                    <tr>
                                        <td>Invoice Number:</td>
                                        <td> {{$shipment->code}} </td>
                                        <td class="text-right"> {{$shipment->code}} </td>
                                        <td style="direction: rtl;">رقم الفاتورة:</td>
                                    </tr>
                                    <tr>
                                    <tr>
                                     <td>Invoice Issue Date:</td>
                                     <td>{{ $shipment->created_at->format('Y-m-d') }}</td>
                                     <td>{{ $shipment->created_at->format('Y-m-d') }}</td>
                                     <td style="direction: rtl;">تاريخ إصدار الفاتورة:</td>
                                  </tr>
                                                                  
                                
                                </table>
                            </div>
                            
                       </div>
                <div class="col-md-4 padding010 " style="float: left">
                      <div class="clearfix"></div>
                        <div class="order_barcodes text-right">

                        <?php $t_p=0; ?>
                        @foreach(\App\PackageShipment::where('shipment_id',$shipment->id)->get() as $package)
                        
                        @foreach(\App\CatalogPackageShipment::with('catalog')->where('package_shipment_id',$package->id)->get() as $cps)
                       <?php  $t_p=$t_p+$cps->price;?>
                        @endforeach
                       
                        @endforeach    
                       
                     

                    
                     <?php 
                        $grand_total=100;
                         $inv_date = date("Y-m-d h:i:s", strtotime($shipment->created_at));
                         $vat_total =($t_p*$shipment->tax_amount)/100;
                         $grand_total =$t_p +$vat_total;
                         $seller = $shipment->client_address;
                         $result = chr(1) . chr( strlen($seller) ) . $seller;
                         $result.= chr(2) . chr( strlen($shipment->branch->vat_no) ) .$shipment->branch->vat_no;
                         $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                         $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                         $result.= chr(5) . chr( strlen( $vat_total ) ) .  $vat_total;
                         $newQr = base64_encode($result); ?>
                         
                         {!! QrCode::size(140)->generate($newQr);!!}
                          
                       
                           
                     </div>
                     </div>
                     <div class="col-xs-12 padding010">
                         <div class="clearfix"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                    <thead>
                                        <tr class="bg-dark">
                                            <th class="text-left" colspan="2">From:</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">من:</th>
                                            <th class="text-left" colspan="2">To:</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">ل:</th>
                                        </tr>
                                </thead>
                                <tbody>
                                 <tr >
                                     <td>Name:</td>
                                     <td colspan="2">{{$shipment->client_address}}</td>
                                     <td style="direction: rtl;">اسم:</td>
                                     <td>Name:</td>
                                     <td  colspan="2">{{$shipment->reciver_address}}</td>
                                     <td style="direction: rtl;">اسم:</td>
                                 </tr>
                                
                                 <tr >
                                     <td>Phone Number :</td>
                                     <td colspan="2">{{$shipment->client_phone}}</td>
                                     <td style="direction: rtl;">رقم التليفون:</td>
                                     <td>Phone Number :</td>
                                     <td  colspan="2">{{$shipment->client_phone}}</td>
                                     <td style="direction: rtl;">رقم التليفون:</td>
                                 </tr>
                                 <tr >
                                     <td>Address :</td>
                                     <td colspan="2">{{$shipment->client_address}}</td>
                                     <td style="direction: rtl;">تبوك:</td>
                                     <td>Address :</td>
                                     <td  colspan="2">{{$shipment->client_address}}</td>
                                     <td style="direction: rtl;">تبوك:</td>
                                 </tr>
                                 <tr >
                                     <td>Country :</td>
                                     <td colspan="2">@if(isset($shipment->from_country->name)){{$shipment->from_country->name}} @endif</td>
                                     <td style="direction: rtl;">دولة:</td>
                                     <td>Country :</td>
                                     <td  colspan="2">@if(isset($shipment->to_country->name)){{$shipment->to_country->name}} @endif</td>
                                     <td style="direction: rtl;">دولة:</td>
                                 </tr>
                                 <tr >
                                     <td>Region :</td>
                                     <td colspan="2">@if(isset($shipment->from_state->name)){{$shipment->from_state->name}} @endif</td>
                                     <td style="direction: rtl;">منطقة:</td>
                                     <td>Region :</td>
                                     <td  colspan="2">@if(isset($shipment->to_state->name)){{$shipment->to_state->name}} @endif</td>
                                     <td style="direction: rtl;">منطقة:</td>
                                 </tr>
                                 
                                
                                </tbody>
                             </table>
                         </div>  
                       </div>
                       
                       <div class="col-xs-12 padding010">
                         <div class="clearfix"></div>
                            <div class="table-responsive">
                               
                            <table class="table table-bordered table-striped table-condensed ">
                                   
                                <thead>
                                    <tr>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Item Name<br>اسم العنصر</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Quantity<br>كمية</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Weight(KG)<br>الوزن (كلغ)</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Taxable Amount<br>المبلغ الخاضع للضريبة</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Tax Rate(%)<br>معدل الضريبة(٪) </th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Tax Amount<br>قيمة الضريبة</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase"> Item Subtotal (Including VAT)<br>المجموع الفرعي للعنصر (بما في ذلك ضريبة القيمة المضافة) </th>
                                       
                                    </tr>
                                    </thead>
                                    <tbody>
                                  <?php   $total_price =0; ?>
                                    @foreach(\App\PackageShipment::where('shipment_id',$shipment->id)->get() as $package)
                                   <tr> <td  class="text-left" colspan="7"><b>@if(isset($package->package->name)){{$package->package->name}} @else - @endif</b></td></tr>
                                   <?php $subtotal_price =0; ?>
                                   @foreach(\App\CatalogPackageShipment::with('catalog')->where('package_shipment_id',$package->id)->get() as $cps)
                                   <?php $total_price=$total_price+$cps->price ;$subtotal_price=$subtotal_price+$cps->price;
                                       
                                   ?>
                                    <tr> <td>{{$cps->catalog->name}}<br>{{$cps->catalog->other_lang}}</td>
                                         <td>{{$cps->qty}}</td>
                                         <td>{{$cps->wgt}}</td>
                                         <td>{{$cps->price}}</td>
                                         <td>{{$shipment->tax_amount}}</td>
                                         <td><?=$tax_amnt =($cps->price*$shipment->tax_amount)/100?>
                                         <td><?=$tax_amnt+$cps->price?></td>
                                         <!-- <td class="text-primary pr-0 pt-7 text-right align-middle">{{$package->weight." ".translate('KG')." x ".$package->length." ".translate('CM')." x ".$package->width." ".translate('CM')." x ".$package->height." ".translate('CM')}}</td> -->
                                    </tr>
                                    @endforeach
                                 
                                    @endforeach
                                    <tr>
                                    <td style="text-align:right;"colspan="5" class="font-weight-bold">Total (Excluding VAT) (USD)</td><td class="font-weight-bold">الإجمالي (باستثناء ضريبة القيمة المضافة) (USD)</td>
                                        <td class="font-weight-bold"><?=$total_price?></td>
                                    </tr>
                                    <tr>
                                    <td style="text-align:right;"colspan="5" class="font-weight-bold">Total VAT</td><td class="font-weight-bold">إجمالي ضريبة القيمة المضافة</td>
                                        <td class="font-weight-bold"><?=$total_vat=($total_price*$shipment->tax_amount)/100?></td>
                                    </tr>
                                    <tr>
                                    <td style="text-align:right;"colspan="5" class="font-weight-bold">Total Amount Due</td><td class="font-weight-bold">إجمالي المبلغ المستحق	</td>
                                        <td class="font-weight-bold"><?= $grand_total = $total_price +$total_vat?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <div class="clearfix"></div>

            </div>
        </div>
     

               
        <!-- end: Invoice body-->
        <!-- begin: Invoice footer-->
        <div class="px-8 py-8 mx-0 bg-gray-100 row justify-content-center py-md-10 px-md-0">
            <div class="col-md-10">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="font-weight-bold text-muted text-uppercase">{{translate('PAYMENT TYPE')}}</th>
                                <th class="font-weight-bold text-muted text-uppercase">{{translate('PAYMENT STATUS')}}</th>
                                <th class="font-weight-bold text-muted text-uppercase">{{translate('PAYMENT DATE')}}</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">{{translate('TOTAL COST')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="font-weight-bolder">
                                <td>{{translate($shipment->pay['name'])}} ({{$shipment->getPaymentType()}})</td>
                                <td>@if($shipment->paid == 1) {{translate('Paid')}} @else {{translate('Pending')}} @endif</td>
                                <td>@if($shipment->paid == 1) {{$shipment->payment->payment_date ?? ""}} @else - @endif</td>
                                <td class="text-right text-primary font-size-h3 font-weight-boldest">{{format_price(convert_price($shipment->tax + $shipment->shipping_cost + $shipment->insurance)) }}<br /><span class="text-muted font-weight-bolder font-size-lg">{{translate('Included tax & insurance')}}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: Invoice footer-->
        <!-- begin: Invoice action-->
        <div class="px-8 py-8 row justify-content-center py-md-10 px-md-0">
            <div class="col-md-10">
                <div class="d-flex justify-content-between">
                    @if($shipment->paid == 0 && $shipment->pay['id'] != 11)
                        <form action="{{ route('payment.checkout') }}" class="form-default" role="form" method="POST" id="checkout-form">
                            @csrf
                            <input type="hidden" name="shipment_id" value="{{$shipment->id}}">
                            <button type="submit" class="mr-3 btn btn-success btn-md">{{translate('Pay Now')}} <i class="ml-2 far fa-credit-card"></i></button>
                        </form>
                        <button class="btn btn-success btn-sm " onclick="copyToClipboard('#payment-link')">{{translate('Copy Payment Link')}}<i class="ml-2 fas fa-copy"></i></button>
                        <div id="payment-link" style="display: none">{{route('admin.shipments.pay', $shipment->id)}}</div>
                    @endif

                    <a href="{{route('admin.shipments.print', array($shipment->id, 'label'))}}" class="btn btn-light-primary font-weight-bold" target="_blank">{{translate('Print Label')}}<i class="ml-2 la la-box-open"></i></a>
                    <a href="{{route('admin.shipments.print', array($shipment->id, 'invoice'))}}" class="btn btn-light-primary font-weight-bold" target="_blank">{{translate('Print Invoice')}}<i class="ml-2 la la-file-invoice-dollar"></i></a>

                    @if(Auth::user()->user_type == 'admin' || in_array('1104', json_decode(Auth::user()->staff->role->permissions ?? "[]")))
                    <a href="{{route('admin.shipments.edit', $shipment->id)}}" class="px-6 py-3 btn btn-light-info btn-sm font-weight-bolder font-size-sm">{{translate('Edit Shipment')}}</a>
                    @endif
                </div>
            </div>
        </div>
        <!-- end: Invoice action-->
        <!-- end: Invoice-->
    </div>
</div>
<!--end::Card-->


<!--end::List Widget 19-->
@if((Auth::user()->user_type == 'admin' || in_array('1102', json_decode(Auth::user()->staff->role->permissions ?? "[]"))) && !empty($shipment->logs->toArray()))
    <div class="card card-custom card-stretch card-stretch-half gutter-b">
        <!--begin::List Widget 19-->

        <!--begin::Header-->
        <div class="pt-6 mb-2 border-0 card-header">
            <h3 class="card-title align-items-start flex-column">
                <span class="mb-3 card-label font-weight-bold font-size-h4 text-dark-75">{{translate('Shipment Status Log')}}</span>

            </h3>
            <div class="card-toolbar">

            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="pt-2 card-body" style="padding-bottom: 0;overflow:hidden">
            <div class="mt-3 timeline timeline-6 scroll scroll-pull" style="overflow:hidden" data-scroll="true" data-wheel-propagation="true">

            @foreach($shipment->logs()->orderBy('id','desc')->get() as $log)
                <!--begin::Item-->
                <div class="timeline-item align-items-start">
                    <!--begin::Label-->
                    <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">{{$log->created_at->diffForHumans()}}</div>
                    <!--end::Label-->

                    <!--begin::Badge-->
                    <div class="timeline-badge">
                        <i class="fa fa-genderless text-warning icon-xl"></i>
                    </div>
                    <!--end::Badge-->

                    <!--begin::Text-->
                    <div class="pl-3 font-weight-mormal font-size-lg timeline-content text-muted">
                        {{translate('Changed from')}}: "{{\App\Shipment::getStatusByStatusId($log->from)}}" {{translate('To')}}: "{{\App\Shipment::getStatusByStatusId($log->to)}}"
                    </div>
                    <!--end::Text-->

                </div>
                <!--end::Item-->

            @endforeach


            </div>
        </div>
    </div>
@endif

@endsection

@section('modal')
@include('modals.delete_modal')
@endsection

@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>
        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
            AIZ.plugins.notify('success', '{{translate("Payment Link Copied")}}');
        }
    </script>
@endsection