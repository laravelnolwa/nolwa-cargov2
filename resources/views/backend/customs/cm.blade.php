
@extends('backend.layouts.app')
@php
    $user_type = Auth::user()->user_type;
    $staff_permission = json_decode(Auth::user()->staff->role->permissions ?? "[]");
    $auth_user = Auth::user();
    $countries = \App\Country::where('covered',1)->get();
@endphp

@section('sub_title'){{translate('Shipments')}}@endsection
@section('subheader')

@endsection

@section('content')
<!--begin::Card-->
<div class="card card-custom gutter-b">
    <div class="flex-wrap py-3 card-header">
        <div class="card-title">
            <h3 class="card-label">
                Customs Manifest
            </h3>
        </div>
        <div class="card-toolbar" id="actions-button">
            <!--begin::Dropdown-->
            <div class="mr-2 dropdown dropdown-inline">
                <button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/PenAndRuller.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
                                <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>{{translate('Actions')}}</button>
                <!--begin::Dropdown Menu-->
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                    <!--begin::Navigation-->
                    <ul class="py-2 navi flex-column navi-hover">
                        <li class="pb-2 navi-header font-weight-bolder text-uppercase font-size-sm text-primary">{{translate('Choose an option:')}}</li>
                        <li class="navi-item">
                        
                                        <a href="#" class="action_checker navi-link action-caller" onclick="openCustomsManifestModel(this,event)" data-url="customs/save_cm" data-method="POST">
                                            <span class="navi-icon">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                            <span class="navi-text">Add</span>
                                        
                                        </a>
                        </li>

                    </ul>
                    <!--end::Navigation-->
                </div>
                <!--end::Dropdown Menu-->
            </div>
         
            <!--end::Dropdown-->
        </div>


    <form id="tableForm">
        @csrf()
        <table class="table mb-0 aiz-table">
            <thead>
                <tr>
                    <th width="3%"></th>
                    <th width="3%">#</th>
                    <th>{{translate('Code')}}</th>
                    @if($status == "all") <th>{{translate('Status')}}</th> @endif
                    <th>{{translate('Type')}}</th>
                    @if($auth_user->user_type != "branch") <th>{{translate('Branch')}}</th> @endif

                    <th>{{translate('Shipping Cost')}}</th>
                    <th>{{translate('Payment Method')}}</th>
                    <th>{{translate('Paid')}}</th>
                    <th>{{translate('Shipping Date')}}</th>
                    @if($status == \App\Shipment::CAPTAIN_ASSIGNED_STATUS || $status == \App\Shipment::RECIVED_STATUS)
                    <th>{{translate('Captain')}}</th>
                    @endif
                    @if($status == \App\Shipment::APPROVED_STATUS || $status == \App\Shipment::CAPTAIN_ASSIGNED_STATUS || $status == \App\Shipment::RECIVED_STATUS)
                    <th>{{translate('Mission')}}</th>
                    @endif
                    <th class="text-center">{{translate('Created At')}}</th>
                    @if($status != "all") <th class="text-center">{{translate('Options')}}</th> @endif
                </tr>
            </thead>
            <tbody>
                @php
                    $client_id = 0;
                @endphp

                @foreach($shipments as $key=>$shipment)
                    @if($client_id != $shipment->client_id)
                        <tr class="bg-light">
                            <td><label class="checkbox checkbox-success"><input type="checkbox" onclick="check_client(this,{{$shipment->client_id}})"/><span></span></label></td>
                            <th colspan="4">
                                @if($user_type == 'admin' || in_array('1100', $staff_permission) || in_array('1005', $staff_permission) )
                                    <a href="{{route('admin.clients.show',$shipment->client_id)}}">{{$shipment->client->name}}</a>
                                @else
                                    {{$shipment->client->name}}
                                @endif
                            </th>
                        </tr>
                        @php
                            $client_id = $shipment->client_id;
                        @endphp
                    @endif
                    
                    <tr>
                        <td>
                            @if($shipment->mission_id)
                                -
                            @else
                                <label class="checkbox checkbox-success"><input data-missionid="{{$shipment->mission_id}}" data-clientaddresssender="{{$shipment->client_address}}" data-clientaddress="{{$shipment->reciver_address}}" data-clientname="{{$shipment->reciver_name}}" data-clientstatehidden="{{$shipment->to_state_id}}" data-clientstate="{{$shipment->to_state->name ?? '' }}" data-clientareahidden="{{$shipment->to_area_id}}" data-clientarea="{{$shipment->to_area->name ?? '' }}" data-clientid="{{$shipment->client->id}}" data-branchid="{{$shipment->branch_id}}" data-branchname="{{$shipment->branch->name}}"  type="checkbox" class="sh-check checkbox-client-id-{{$shipment->client_id}}" name="checked_ids[]" value="{{$shipment->id}}" /><span></span></label>
                            @endif
                        </td>
                        @if($user_type == 'admin' || in_array('1100', $staff_permission) || in_array('1009', $staff_permission) )
                            <td width="3%"><a href="{{route('admin.shipments.show', ['shipment'=>$shipment->id])}}">{{ ($key+1) + ($shipments->currentPage() - 1)*$shipments->perPage() }}</a></td>
                            <td width="5%"><a href="{{route('admin.shipments.show', ['shipment'=>$shipment->id])}}">{{$shipment->code}}</a></td>
                        @else
                            <td width="3%">{{ ($key+1) + ($shipments->currentPage() - 1)*$shipments->perPage() }}</td>
                            <td width="5%">{{$shipment->code}}</td>
                        @endif
                        
                        @if($status == "all") <td>{{$shipment->getStatus()}}</td> @endif
                        <td>{{$shipment->type}}</td>
                        @if( in_array($user_type ,['admin','customer']) || in_array('1100', $staff_permission) || in_array('1006', $staff_permission) )
                            <td><a href="{{route('admin.branchs.show',$shipment->branch_id)}}">{{$shipment->branch->name}}</a></td>
                        @else
                            <td>{{$shipment->branch->name}}</td>
                        @endif

                        <td>{{format_price(convert_price($shipment->tax + $shipment->shipping_cost + $shipment->insurance)) }}</td>
                        <td>{{$shipment->pay->name ?? ""}}</td>
                        <td>@if($shipment->paid == 1) {{translate('Paid')}} @else - @endif</td>
                        <td>{{$shipment->shipping_date}}</td>
                            @if($status == \App\Shipment::CAPTAIN_ASSIGNED_STATUS || $status == \App\Shipment::RECIVED_STATUS)
                                <td><a href="{{route('admin.captains.show', $shipment->captain_id)}}">@isset($shipment->captain_id) {{$shipment->captain->name}} @endisset</a></td>
                            @endif
                        @if($status == \App\Shipment::APPROVED_STATUS || $status == \App\Shipment::CAPTAIN_ASSIGNED_STATUS || $status == \App\Shipment::RECIVED_STATUS )
                            <td>@isset($shipment->current_mission->id) <a href="{{route('admin.missions.show', $shipment->current_mission->id)}}"> {{$shipment->current_mission->code}}</a> @endisset</td>
                        @endif
                        <td class="text-center">
                            {{$shipment->created_at->format('Y-m-d')}}
                        </td>
                        @if($status != "all") 
                            <td class="text-center">
                                <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.shipments.print', ['shipment'=>$shipment->id, 'invoice'])}}" title="{{ translate('Show') }}">
                                    <i class="las la-print"></i>
                                </a>
                                <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.shipments.show', $shipment->id)}}" title="{{ translate('Show') }}">
                                    <i class="las la-eye"></i>
                                </a>

                                @if($status != \App\Shipment::APPROVED_STATUS && $status != \App\Shipment::CAPTAIN_ASSIGNED_STATUS && $status != \App\Shipment::CLOSED_STATUS && $status != \App\Shipment::RECIVED_STATUS && $status != \App\Shipment::IN_STOCK_STATUS && $status != \App\Shipment::DELIVERED_STATUS && $status != \App\Shipment::SUPPLIED_STATUS && $status != \App\Shipment::RETURNED_STATUS )
                                    <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.shipments.edit', $shipment->id)}}" title="{{ translate('Edit') }}">
                                        <i class="las la-edit"></i>
                                    </a>
                                @endif

                            </td> 
                        @endif
                    </tr>

                @endforeach

            </tbody>
        </table>

        <!-- Assign-to-captain Modal -->

        <div id="customs-manifest-modal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title h6">{{translate('Create Customs Manifest')}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('From Country')}}:</label>
                                    <select id="change-country" name="from_country_id" class="form-control select-country">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('To Country')}}:</label>
                                    <select id="change-country-to" name="to_country_id" class="form-control select-country">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
</div>
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Container No:')}}:</label>
                                    <input type="text" placeholder="{{translate('Container No')}}" name="container_no" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Passport No')}}:</label>
                                    <input type="text" placeholder="{{translate('Passport No')}}" name="passport_no" class="form-control" />
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Shipment Type')}}:</label>
                                    <select id="change-country" name="shipment_type_id" class="form-control select-country">
                                        <option value=""></option>
                                        <option value="1">Air Cargo</option>
                                        <option value="1">Sea Cargo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Name')}}:</label>
                                    <input type="text" placeholder="{{translate('Name')}}" name="name" class="form-control" />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{translate('Close')}}</button>
                        <input type="submit" class="btn btn-primary"  value="{{translate('Create Customs Manifest')}}" id="submit_transfer"/>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="aiz-pagination">
        {{ $shipments->appends(request()->input())->links() }}
    </div>
</div>
</div>
{!! hookView('shipment_addon',$currentView) !!}

@endsection

@section('modal')
@include('modals.delete_modal')
@endsection

@section('script')
<script type="text/javascript">
    // $(document).on('click','#submit_transfer',function(){
    //     $('#tableForm').submit();
    // });
  
    //customs manifest
    function openCustomsManifestModel(element, e) {
        var selected = [];
        var selected_address_sender = [];
        var selected_address = [];
        var selected_branch_hidden = [];
        var mission_id = [];
        $('.sh-check:checked').each(function() {
            mission_id.push($(this).data('missionid'));
        });

        console.log(mission_id);
        if(mission_id.length != 0){
               $('#tableForm').attr('action', $(element).data('url'));
                $('#tableForm').attr('method', $(element).data('method'));
              $('#customs-manifest-modal').modal('toggle');
            } else
                Swal.fire("{{translate('Please Select Shipments')}}", "", "error");
        
    }
  

    // $(document).ready(function() {
    //     $('.action-caller').on('click', function(e) {
    //         e.preventDefault();
    //         var selected = [];
    //         $('.sh-check:checked').each(function() {
    //             selected.push($(this).data('clientid'));
    //         });
    //         if (selected.length > 0) {
    //             $('#tableForm').attr('action', $(this).data('url'));
    //             $('#tableForm').attr('method', $(this).data('method'));
    //             $('#tableForm').submit();
    //         } else if (selected.length == 0) {
    //             Swal.fire("{{translate('Please Select Shipments action')}}", "", "error");
    //         }

    //     });

        // FormValidation.formValidation(
        //     document.getElementById('tableForm'), {
        //         fields: {
        //             "Mission[address]": {
        //                 validators: {
        //                     notEmpty: {
        //                         message: '{{translate("This is required!")}}'
        //                     }
        //                 }
        //             },
        //             "Mission[client_id]": {
        //                 validators: {
        //                     notEmpty: {
        //                         message: '{{translate("This is required!")}}'
        //                     }
        //                 }
        //             },
        //             "Mission[to_branch_id]": {
        //                 validators: {
        //                     notEmpty: {
        //                         message: '{{translate("This is required!")}}'
        //                     }
        //                 }
        //             }


        //         },


        //         plugins: {
        //             autoFocus: new FormValidation.plugins.AutoFocus(),
        //             trigger: new FormValidation.plugins.Trigger(),
        //             // Bootstrap Framework Integration
        //             bootstrap: new FormValidation.plugins.Bootstrap(),
        //             // Validate fields when clicking the Submit button
        //             submitButton: new FormValidation.plugins.SubmitButton(),
        //             // Submit the form when all fields are valid
        //             defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        //             icon: new FormValidation.plugins.Icon({
        //                 valid: 'fa fa-check',
        //                 invalid: 'fa fa-times',
        //                 validating: 'fa fa-refresh',
        //             }),
        //         }
        //     }
        // );

   // });
</script>

@endsection
