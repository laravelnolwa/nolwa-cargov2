<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customs extends Model
{
    protected $guarded = [];
    public function fromCountry(){
        return $this->belongsTo(Country::class,'from_country_id');
    }
    public function toCountry(){
        return $this->belongsTo(Country::class,'to_country_id');
    }
}

