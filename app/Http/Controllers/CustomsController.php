<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shipment;
use App\Customs;
use App\Country;
use App\CustomsShipment;
use App\CustomsShipmentView;
use Auth;
use App\Http\Helpers\ShipmentActionHelper;

use Illuminate\Support\Facades\DB;

class CustomsController extends Controller
{
    public function index(){
        $shipments = new Shipment();
        $type = null;
        if (isset($_GET)) {
            if (isset($_GET['code']) && !empty($_GET['code'])) {

                $shipments = $shipments->where('code', $_GET['code']);
            }
        }
        $shipments = $shipments->orderBy('id','DESC');
    
    return view('backend.customs.index',compact('shipments'));
        }

        public function create(){
            return view('backend.customs.create');
        }
        public function store(Request $request)
        {
            try{	
                DB::beginTransaction();
               
                $model = new Customs();
                
                
                $model->fill($request->all());
              
                if (!$model->save()){
                    throw new \Exception();
                }
                
                DB::commit();
               // dd($model);
                flash(translate("Customs Manifest added successfully"))->success();
              //  $CustomsShipmentView = CustomsShipmentView::where(['from_country_id',$model->from_country_id],['to_country_id',$model->to_country_id]);
               // ->where('from_country_id',$model->from_country_id);
               $CustomsShipmentView = CustomsShipmentView::all();
                return view('backend.customs.addshipment',compact('model','CustomsShipmentView'));
                
            }catch(\Exception $e){
                DB::rollback();
                print_r($e->getMessage());
                exit;
                
                flash(translate("Error"))->error();
                return back();
            }
        }
        public function addShipment($data){
            $customs = Customs::find($data->id);
             
        }
        public function cm(){
            $type = null;
            $shipments = new Shipment();
        if (isset($_GET)) {
            if (isset($_GET['code']) && !empty($_GET['code'])) {

                $shipments = $shipments->where('code', $_GET['code']);
            }
            if (isset($_GET['client_id']) && !empty($_GET['client_id'])) {

                $shipments = $shipments->where('client_id', $_GET['client_id']);
            }
            if (isset($_GET['branch_id']) && !empty($_GET['branch_id'])) {
                $shipments = $shipments->where('branch_id', $_GET['branch_id']);
            }
            if (isset($_GET['type']) && !empty($_GET['type'])) {
                $shipments = $shipments->where('type', $_GET['type']);
            }
        }
        if(Auth::user()->user_type == 'customer'){
            $shipments = $shipments->where('client_id', Auth::user()->userClient->client_id);
        }elseif(Auth::user()->user_type == 'branch'){
            $shipments = $shipments->where('branch_id', Auth::user()->userBranch->branch_id);
        }
        $shipments = $shipments->with('pay')->orderBy('client_id')->orderBy('id','DESC')->paginate(20);
        $actions = new ShipmentActionHelper();
        $actions = $actions->get('all');
        $page_name = translate('All Shipments');
        $status = 'all';
           // return view('backend.shipments.index', compact('shipments', 'page_name', 'type', 'actions', 'status'));
           // $CustomsShipmentView = CustomsShipmentView::all();
            return view('backend.customs.cm',compact('shipments','page_name', 'type', 'actions', 'status'));
             
        }
        public function saveCM(Request $request){
         //   dd(checked_ids);
            try{	
                DB::beginTransaction();
               
                $model = new Customs();
                
                $model->from_country_id = $request->from_country_id;
                $model->to_country_id = $request->to_country_id;
                $model->shipment_type_id = $request->shipment_type_id;
                $model->container_no = $request->container_no;
                $model->passport_no = $request->passport_no;
                $model->name = $request->name;
                
               // $model->fill($request->all());
              
                if (!$model->save()){
                    throw new \Exception();
                }
                
                DB::commit();
              
                flash(translate("Customs Manifest added successfully"))->success();
                foreach ($request->checked_ids as $id){
                     CustomsShipment::create([
                       'shipment_id'=>$id,
                       'customs_id'=>$model->id
                    ]);
                }
                $customs= Customs::with('fromCountry','toCountry')->find($model->id);
                $missions = Shipment::with('to_state')->whereIn('id',$request->checked_ids)->get();
                return view('backend.customs.print_cm',compact('missions','customs'));  



              //  $CustomsShipmentView = CustomsShipmentView::where(['from_country_id',$model->from_country_id],['to_country_id',$model->to_country_id]);
               // ->where('from_country_id',$model->from_country_id);
              // $CustomsShipmentView = CustomsShipmentView::all();
               // return view('backend.customs.addshipment',compact('model','CustomsShipmentView'));

                
            }catch(\Exception $e){
                DB::rollback();
                print_r($e->getMessage());
                exit;
                
                flash(translate("Error"))->error();
                return back();
            }

        }
        
        

        
}
