<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomsShipmentView extends Model
{
    protected $table = "customs_shipment_view";
}
